
package com.info.mvcstudentmanagement.utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
    
    static public Connection getConnection(){
        
        try{
             //load driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            //Establish Connection
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mvc_student", "root", "toor");
            
            return con;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
