
package com.info.mvcstudentmanagement.model;


public class Student {
    
    private int studentId;
    private String studentName;
    private String studentAddress;
    private Object studentImage;

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    /**
     * @return the studentAddress
     */
    public String getStudentAddress() {
        return studentAddress;
    }

    /**
     * @param studentAddress the studentAddress to set
     */
    public void setStudentAddress(String studentAddress) {
        this.studentAddress = studentAddress;
    }

    /**
     * @return the studentImage
     */
    public Object getStudentImage() {
        return studentImage;
    }

    /**
     * @param studentImage the studentImage to set
     */
    public void setStudentImage(Object studentImage) {
        this.studentImage = studentImage;
    }
    
}
